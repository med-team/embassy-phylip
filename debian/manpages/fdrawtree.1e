'\" t
.\"     Title: FDRAWTREE
.\"    Author: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
.\" Generator: DocBook XSL Stylesheets v1.75.2 <http://docbook.sf.net/>
.\"      Date: 08/11/2010
.\"    Manual: EMBOSS Manual for Debian
.\"    Source: PHYLIPNEW 3.69+20100721
.\"  Language: English
.\"
.TH "FDRAWTREE" "1e" "08/11/2010" "PHYLIPNEW 3.69+20100721" "EMBOSS Manual for Debian"
.\" -----------------------------------------------------------------
.\" * Define some portability stuff
.\" -----------------------------------------------------------------
.\" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.\" http://bugs.debian.org/507673
.\" http://lists.gnu.org/archive/html/groff/2009-02/msg00013.html
.\" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.ie \n(.g .ds Aq \(aq
.el       .ds Aq '
.\" -----------------------------------------------------------------
.\" * set default formatting
.\" -----------------------------------------------------------------
.\" disable hyphenation
.nh
.\" disable justification (adjust text to left margin only)
.ad l
.\" -----------------------------------------------------------------
.\" * MAIN CONTENT STARTS HERE *
.\" -----------------------------------------------------------------
.SH "NAME"
fdrawtree \- Plots an unrooted tree diagram
.SH "SYNOPSIS"
.HP \w'\fBfdrawtree\fR\ 'u
\fBfdrawtree\fR \fB\-fontfile\ \fR\fB\fIstring\fR\fR \fB\-intreefile\ \fR\fB\fItree\fR\fR \fB\-plotfile\ \fR\fB\fIoutfile\fR\fR [\fB\-plotter\ \fR\fB\fIlist\fR\fR] [\fB\-previewer\ \fR\fB\fIlist\fR\fR] [\fB\-iterate\ \fR\fB\fIlist\fR\fR] [\fB\-lengths\ \fR\fB\fIboolean\fR\fR] [\fB\-labeldirection\ \fR\fB\fIlist\fR\fR] [\fB\-treeangle\ \fR\fB\fIfloat\fR\fR] [\fB\-arc\ \fR\fB\fIfloat\fR\fR] \fB\-labelrotation\ \fR\fB\fIfloat\fR\fR [\fB\-rescaled\ \fR\fB\fItoggle\fR\fR] \fB\-bscale\ \fR\fB\fIfloat\fR\fR [\fB\-treedepth\ \fR\fB\fIfloat\fR\fR] \fB\-xmargin\ \fR\fB\fIfloat\fR\fR \fB\-ymargin\ \fR\fB\fIfloat\fR\fR \fB\-xrayshade\ \fR\fB\fIfloat\fR\fR \fB\-yrayshade\ \fR\fB\fIfloat\fR\fR [\fB\-paperx\ \fR\fB\fIfloat\fR\fR] [\fB\-papery\ \fR\fB\fIfloat\fR\fR] [\fB\-pagesheight\ \fR\fB\fIfloat\fR\fR] [\fB\-pageswidth\ \fR\fB\fIfloat\fR\fR] [\fB\-hpmargin\ \fR\fB\fIfloat\fR\fR] [\fB\-vpmargin\ \fR\fB\fIfloat\fR\fR]
.HP \w'\fBfdrawtree\fR\ 'u
\fBfdrawtree\fR \fB\-help\fR
.SH "DESCRIPTION"
.PP
\fBfdrawtree\fR
is a command line program from EMBOSS (\(lqthe European Molecular Biology Open Software Suite\(rq)\&. It is part of the "Phylogeny:Tree drawing" command group(s)\&.
.SH "OPTIONS"
.SS "Input section"
.PP
\fB\-fontfile\fR \fIstring\fR
.RS 4
Default value: font1
.RE
.PP
\fB\-intreefile\fR \fItree\fR
.RS 4
.RE
.SS "Additional section"
.SS "Output section"
.PP
\fB\-plotfile\fR \fIoutfile\fR
.RS 4
.RE
.PP
\fB\-plotter\fR \fIlist\fR
.RS 4
Default value: l
.RE
.PP
\fB\-previewer\fR \fIlist\fR
.RS 4
Default value: x
.RE
.PP
\fB\-iterate\fR \fIlist\fR
.RS 4
Default value: e
.RE
.PP
\fB\-lengths\fR \fIboolean\fR
.RS 4
Default value: N
.RE
.PP
\fB\-labeldirection\fR \fIlist\fR
.RS 4
Default value: m
.RE
.PP
\fB\-treeangle\fR \fIfloat\fR
.RS 4
Default value: 90\&.0
.RE
.PP
\fB\-arc\fR \fIfloat\fR
.RS 4
Default value: 360
.RE
.PP
\fB\-labelrotation\fR \fIfloat\fR
.RS 4
Default value: 90\&.0
.RE
.PP
\fB\-rescaled\fR \fItoggle\fR
.RS 4
Default value: Y
.RE
.PP
\fB\-bscale\fR \fIfloat\fR
.RS 4
Default value: 1\&.0
.RE
.PP
\fB\-treedepth\fR \fIfloat\fR
.RS 4
Default value: 0\&.53
.RE
.PP
\fB\-xmargin\fR \fIfloat\fR
.RS 4
Default value: 1\&.65
.RE
.PP
\fB\-ymargin\fR \fIfloat\fR
.RS 4
Default value: 2\&.16
.RE
.PP
\fB\-xrayshade\fR \fIfloat\fR
.RS 4
Default value: 1\&.65
.RE
.PP
\fB\-yrayshade\fR \fIfloat\fR
.RS 4
Default value: 2\&.16
.RE
.PP
\fB\-paperx\fR \fIfloat\fR
.RS 4
Default value: 20\&.63750
.RE
.PP
\fB\-papery\fR \fIfloat\fR
.RS 4
Default value: 26\&.98750
.RE
.PP
\fB\-pagesheight\fR \fIfloat\fR
.RS 4
Default value: 1
.RE
.PP
\fB\-pageswidth\fR \fIfloat\fR
.RS 4
Default value: 1
.RE
.PP
\fB\-hpmargin\fR \fIfloat\fR
.RS 4
Default value: 0\&.41275
.RE
.PP
\fB\-vpmargin\fR \fIfloat\fR
.RS 4
Default value: 0\&.53975
.RE
.SH "BUGS"
.PP
Bugs can be reported to the Debian Bug Tracking system (http://bugs\&.debian\&.org/emboss), or directly to the EMBOSS developers (http://sourceforge\&.net/tracker/?group_id=93650&atid=605031)\&.
.SH "SEE ALSO"
.PP
fdrawtree is fully documented via the
\fBtfm\fR(1)
system\&.
.SH "AUTHOR"
.PP
\fBDebian Med Packaging Team\fR <\&debian\-med\-packaging@lists\&.alioth\&.debian\&.org\&>
.RS 4
Wrote the script used to autogenerate this manual page\&.
.RE
.SH "COPYRIGHT"
.br
.PP
This manual page was autogenerated from an Ajax Control Definition of the EMBOSS package\&. It can be redistributed under the same terms as EMBOSS itself\&.
.sp
